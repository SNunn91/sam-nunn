# Prismic JSON

**This directory is not required, you can delete it if you don't want to use it.**

This directory contains examples of JSON that can be used directly in the Prismic CMS as starting points.
