import Prismic from 'prismic-javascript'
import config from '@/config'
/* ----------------------------------------------------------------------
| Get Homepage Data
|------------------------------------------------------------------------ */

/* Mutations - Makes the changes */
export const mutations = {
  SET_HOME(state, value) {
    state.document = value
  }
}
/* Actions - Triggers the changes */
export const actions = {
  async GET_HOME({ commit, req }) {
    const api = await Prismic.getApi(config.prismic.endpoint, req)
    const returned = await api.getSingle('homepage')
    commit('SET_HOME', returned)
  }
}
