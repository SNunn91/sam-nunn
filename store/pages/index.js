import Prismic from 'prismic-javascript'
import config from '@/config'
/* ----------------------------------------------------------------------
| Get all Pages
|------------------------------------------------------------------------ */

/* Default State */
export const state = () => ({
  documents: []
})

/* Mutations - Makes the changes */
export const mutations = {
  SET_PAGES(state, value) {
    state.documents = value
  }
}
/* Actions - Triggers the changes */
export const actions = {
  async GET_PAGES({ commit, req }) {
    const api = await Prismic.getApi(config.prismic.endpoint, req)
    const returned = await api.query(
      Prismic.Predicates.at('document.type', 'page')
    )
    commit('SET_PAGES', returned.results)
  }
}
