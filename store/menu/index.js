export const state = () => ({
  open: false
})

/* Mutations - Makes the changes */
export const mutations = {
  SET_MENU_OPEN(state, value) {
    state.open = !state.open
  }
}

/* Actions - Triggers the changes */
export const actions = {
  TRIGGER_MENU_OPEN({ commit }, value) {
    commit('SET_MENU_OPEN', value)
  }
}
