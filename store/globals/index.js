import Prismic from 'prismic-javascript'
import config from '@/config'
/* ----------------------------------------------------------------------
| Get all Global Information
|------------------------------------------------------------------------ */

/* Mutations - Makes the changes */
export const mutations = {
  SET_GLOBALS(state, value) {
    state.document = value.data
  }
}
/* Actions - Triggers the changes */
export const actions = {
  async GET_GLOBALS({ commit, req }) {
    const api = await Prismic.getApi(config.prismic.endpoint, req)
    const returned = await api.getSingle('globals')
    commit('SET_GLOBALS', returned)
  }
}
