export const actions = {
  async nuxtServerInit({ dispatch }) {
    await dispatch('globals/GET_GLOBALS')
    await dispatch('home/GET_HOME')
    await dispatch('pages/GET_PAGES')
  }
}
