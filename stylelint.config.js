module.exports = {
  // add your custom config here
  // https://stylelint.io/user-guide/configuration
  plugins: ['stylelint-order', 'stylelint-config-rational-order/plugin'],
  rules: {
    indentation: 2,
    'color-no-invalid-hex': true,
    'color-named': 'never',
    'selector-no-qualifying-type': true,
    'function-calc-no-invalid': true,
    'block-no-empty': true,
    'unit-no-unknown': true,
    'property-no-unknown': true,
    'comment-no-empty': true,
    'function-name-case': 'lower',
    'rule-empty-line-before': 'always-multi-line',
    'declaration-empty-line-before': 'never',
    'no-extra-semicolons': true,
    'no-duplicate-selectors': true,
    'no-duplicate-at-import-rules': true,
    'shorthand-property-no-redundant-values': true,
    'value-no-vendor-prefix': true,
    'max-nesting-depth': 2,
    'length-zero-no-unit': true,
    'no-missing-end-of-source-newline': true,
    'order/properties-order': [],
    'plugin/rational-order': [
      true,
      {
        'border-in-box-model': false,
        'empty-line-between-groups': false
      }
    ]
  }
}
