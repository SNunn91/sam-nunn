/* ----------------------------------------------------------------------
| Get Global Settings
|------------------------------------------------------------------------
| This file loads up settings from the .env file, so that we can use them safely around the site.
| e.g import config from '@/config'
| Then use like a normal object, i.e config.prismic.endpoint
*/

export default {
  prismic: {
    endpoint: 'https://sam-nunn-portfolio.cdn.prismic.io/api/v2',
    repo: '',
    enabled: false
  },
  meta: {
    title: 'This is the meta title',
    description: 'This is the meta description'
  },
  dynamic_routes: {
    page: 'page',
    flooring_type: 'flooring-type'
  },
  copyright: 'Example Company Name. All Rights Reserved.',
  analytics: {
    hotjar: {
      enabled: false,
      id: 1695714
    },
    facebook: {
      enabled: false,
      id: 129300201740346
    },
    google: {
      enabled: false,
      id: 'UA-149220229-1'
    }
  }
}
