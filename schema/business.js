const business = {
  '@context': 'http://www.schema.org',
  '@type': 'LocalBusiness',
  name: 'kith + kin',
  url: 'https://kithkin.studio/',
  logo: 'https://kithkin.studio/',
  image: 'https://kithkin.studio/',
  description:
    'We are a full service design studio with a strong focus on creating meaningful Brands, Websites + Shopify eCommerce stores.',
  address: {
    '@type': 'PostalAddress',
    streetAddress: '10 North Lynn Business Village, Bergen Way',
    addressLocality: "King's Lynn",
    addressRegion: 'Norfolk',
    postalCode: 'PE30 2JG',
    addressCountry: 'United Kingdom'
  },
  telephone: '+44 (0)1553 605850',
  openingHours: 'Mo,Tu,We,Th,Fr 09:00-17:00',
  geo: {
    '@type': 'GeoCoordinates',
    latitude: '52.7721674',
    longitude: '0.4159156'
  },
  priceRange: '£1,000 and up',
  sameAs: [
    'https://www.facebook.com/kandkin/',
    'https://www.instagram.com/kandkin/',
    'https://www.linkedin.com/company/kith-kin/'
  ]
}

export { business }
