import config from '../config.js'

const Prismic = require('prismic-javascript')
const promises = []

Object.keys(config.dynamic_routes).forEach((type) => {
  const promise = Prismic.getApi(config.prismic.endpoint)
    .then((api) =>
      api.query(Prismic.Predicates.at('document.type', type), { pageSize: 100 })
    )
    .then((res) =>
      res.results.map((post) => `/${config.dynamic_routes[type]}/${post.uid}/`)
    )
  promises.push(promise)
})

const list = Promise.all(promises).then((values) => {
  return values
})

console.log(list)

export default list
