import config from '../config.js'
import { business } from '../schema/business'

const scripts = []

if (config.analytics.google.enabled) {
  scripts.push({
    src: `//www.googletagmanager.com/gtag/js?id=${config.analytics.google.id}`,
    async: true
  })
}

if (config.prismic.enabled) {
  scripts.push({
    src: `//static.cdn.prismic.io/prismic.js?repo=${config.prismic.repo}&new=true`,
    defer: true,
    async: true
  })
}

scripts.push({
  type: 'application/ld+json',
  json: business
})

export default scripts
