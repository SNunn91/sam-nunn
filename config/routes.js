import config from '../config.js'

const Prismic = require('prismic-javascript')

async function thingy() {
  const promises = []

  Object.keys(config.dynamic_routes).forEach((type) => {
    const promise = Prismic.getApi(config.prismic.endpoint)
      .then((api) =>
        api.query(Prismic.Predicates.at('document.type', type), {
          pageSize: 100
        })
      )
      .then((res) =>
        res.results.map(
          (post) => `/${config.dynamic_routes[type]}/${post.uid}/`
        )
      )
    promises.push(promise)
  })

  await Promise.all(promises).then((values) => {
    return values
  })
}
export function thingy()
