import config from './config.js'
import scripts from './config/scripts'
// import routes from './config/routes'

export default {
  mode: 'universal',
  /*
   ** Headers of the page
   */
  pageTransition: {
    name: 'PageFade',
    mode: 'out-in'
  },
  head: {
    title: config.meta.title,
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      {
        hid: 'description',
        name: 'description',
        content: config.meta.description
      }
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' },
      {
        rel: 'preconnect',
        href: 'https://images.prismic.io/',
        crossorigin: true
      }
    ],
    script: scripts
  },
  /*
   ** Customize the progress-bar color
   */
  loading: { color: '#fff' },
  /*
   ** Global CSS
   */
  css: ['~/assets/pcss/site.pcss'],
  /*
   ** Plugins to load before mounting the App
   */
  plugins: [
    /* Prismic */
    '~/plugins/prismic/link-resolver',
    '~/plugins/prismic/html-serializer',
    '~/plugins/prismic/prismic-vue',
    /* Components */
    '~/plugins/components/global-components',
    /* Filters */
    '~/plugins/filters/filters',
    /* Analytics */
    // { src: '~/plugins/analytics/google', mode: 'client' },
    // { src: '~/plugins/analytics/facebook', mode: 'client' },
    // { src: '~/plugins/analytics/hotjar', mode: 'client' },
    /* Client Side Only */
    { src: '~/plugins/lazysizes/lazysizes', mode: 'client' }
    // { src: '~/plugins/kith-and-kin/greeting', mode: 'client' }
  ],
  /*
   ** Nuxt.js dev-modules
   */
  buildModules: [
    // Doc: https://github.com/nuxt-community/eslint-module
    '@nuxtjs/eslint-module',
    // Doc: https://github.com/nuxt-community/stylelint-module
    [
      '@nuxtjs/stylelint-module',
      {
        files: ['assets/**/*.{s?(a|c)ss,less,stylus,pcss}'],
        fix: true
      }
    ]
  ],
  /*
   ** Nuxt.js modules
   */
  modules: [
    // Doc: https://axios.nuxtjs.org/usage
    '@nuxtjs/axios',
    // Allows SVGs to be imported inline
    '@nuxtjs/svg'
  ],
  /*
   ** Axios module configuration
   ** See https://axios.nuxtjs.org/options
   */
  axios: {
    baseURL: '/'
  },
  /*
   ** Build configuration
   */
  generate: {
    // routes() {}
  },
  build: {
    /*
     ** You can extend webpack config here
     */
    transpile: ['gsap'],
    extend(config, ctx) {
      config.resolve.alias.vue = 'vue/dist/vue.common'
    },
    postcss: {
      // Add plugin names as key and arguments as value
      // Install them before as dependencies with npm or yarn
      plugins: {
        // Disable a plugin by passing false as value
        'postcss-preset-env': {},
        'postcss-sassy-mixins': {},
        'postcss-property-lookup': {},
        'postcss-custom-media': {},
        'postcss-round-subpixels': {},
        'postcss-media-minmax': {},
        'postcss-nested': { unwrap: ['media'] }
      },
      preset: {
        // Change the postcss-preset-env settings
        autoprefixer: {
          grid: true
        }
      }
    }
  },
  router: {
    trailingSlash: true
  }
}
