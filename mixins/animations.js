import { gsap } from 'gsap'

export default {
  methods: {
    appear() {
      const timeline = gsap.timeline()
      const set = document.querySelectorAll('[data-appear]')
      timeline.to(set, {
        stagger: 0.7,
        delay: 0.7,
        ease: 'power2.inOut',
        duration: 1.5,
        opacity: 1
      })
    }
  }
}
