import Vue from 'vue'

/*
|--------------------------------------------------------------------------
| Widows Filter
|--------------------------------------------------------------------------
*/

Vue.filter('widow', function(value) {
  if (!value) return ''
  value = value.toString()
  return value.replace(/\s([^\s<]+)\s*$/, '\u00A0$1')
})

/*
|--------------------------------------------------------------------------
| Strip Whitespace
|--------------------------------------------------------------------------
*/

Vue.filter('remove_whitespace', function(value) {
  if (!value) return ''
  value = value.toString()
  return value.replace(/\s/g, '')
})
