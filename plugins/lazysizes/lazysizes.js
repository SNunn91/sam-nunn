/*
|--------------------------------------------------------------------------
| Lazyloading of images
|--------------------------------------------------------------------------
*/
import lazysizes from 'lazysizes'
import 'lazysizes/plugins/respimg/ls.respimg.js'
import 'lazysizes/plugins/rias/ls.rias'
import 'lazysizes/plugins/blur-up/ls.blur-up.js'
import 'lazysizes/plugins/object-fit/ls.object-fit.js'
import 'lazysizes/plugins/parent-fit/ls.parent-fit.js'
import 'lazysizes/plugins/attrchange/ls.attrchange'

lazysizes.cfg.blurupMode = 'auto'
lazysizes.cfg.throttleDelay = 200
