import Vue from 'vue'

/* Layout */
import Header from '@/components/layout/Header'
import Footer from '@/components/layout/Footer'

/* UI */
import Burger from '@/components/global/navigation/Burger.vue'
import Menu from '@/components/global/navigation/Menu.vue'

/* Images */
import PrismicImage from '@/components/global/image/PrismicImage'

/* Contact Methods */
import GlobalEmail from '@/components/global/contact/Email'

/* Social */
import GlobalLinkedin from '@/components/global/social/Linkedin'
import GlobalGit from '@/components/global/social/Git'
import GlobalIconEmail from '@/components/global/social/Email'

Vue.component('v-style', {
  render(createElement) {
    return createElement('style', this.$slots.default)
  }
})
/*
|--------------------------------------------------------------------------
| Global components
|--------------------------------------------------------------------------
| These are components it makes more sense to import globally.
*/

/* Layout */
Vue.component('Header', Header)
Vue.component('Footer', Footer)

/* UI */
Vue.component('Burger', Burger)
Vue.component('Menu', Menu)

/* Images */
Vue.component('PrismicImage', PrismicImage)

/* Contact Methods */
Vue.component('GlobalEmail', GlobalEmail)

/* Social Methods */
Vue.component('GlobalLinkedin', GlobalLinkedin)
Vue.component('GlobalGit', GlobalGit)
Vue.component('GlobalIconEmail', GlobalIconEmail)
