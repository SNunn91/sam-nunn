import config from '@/config'

/* Global site tag (gtag.js) - Google Analytics */

if (config.analytics.google.enabled) {
  /* eslint-disable */
  window.dataLayer = window.dataLayer || []
  function gtag() {
    dataLayer.push(arguments)
  }
  gtag('js', new Date())

  gtag('config', config.analytics.google.id)
    /* eslint-enable */
}
